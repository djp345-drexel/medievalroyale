﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UpdateText : MonoBehaviour {

    TMP_Text text;

	// Use this for initialization
	void Start ()
    {
        text = GetComponent<TextMeshProUGUI>();
	}
	
	// Update is called once per frame
	public void textUpdate(float value)
    {
        if (gameObject.tag == "Bar")
        {
            text.text = Mathf.RoundToInt(value * 100) + "";
        }
        
	}
}
