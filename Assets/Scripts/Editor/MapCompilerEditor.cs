﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

public class MapCompilerEditor : EditorWindow {

    private string path = "Assets/Maps/map.json";

    [MenuItem("Server/MapCompiler")]
	public static void ShowWindow()
    {
        EditorWindow.GetWindow(typeof(MapCompilerEditor));
    }

    private void OnGUI()
    {
        GUILayout.Label("Server Map Compiler");
        string path = GUILayout.TextField(this.path);
        if (GUILayout.Button("Compile Map"))
        {
            CompileMap(path);
        }
    }

    public void CompileMap(string path)
    {
        List<ChestJSON> chestList = new List<ChestJSON>();
        foreach (GameObject gameObj in UnityEngine.Object.FindObjectsOfType<GameObject>())
        {
            if (gameObj.name == "NetworkedObjects")
            {
                foreach (Transform netTrans in gameObj.transform)
                {
                    if (netTrans.name == "NetworkedChest")
                    {
                        ChestJSON json = new ChestJSON();
                        json.id = -1;
                        json.position = new float[2];
                        json.position[0] = netTrans.position.x;
                        json.position[1] = netTrans.position.y;
                        chestList.Add(json);
                    }
                }
                break;
            }
        }

        MapJSON mapJSON = new MapJSON();
        mapJSON.chests = chestList.ToArray();

        string mapString = JsonUtility.ToJson(mapJSON);

        using (FileStream fs = new FileStream(path, FileMode.Create))
        {
            using (StreamWriter writer = new StreamWriter(fs))
            {
                writer.Write(mapString);
            }
        }
        UnityEditor.AssetDatabase.Refresh();
    }
}
