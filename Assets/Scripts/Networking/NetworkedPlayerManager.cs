﻿using Quobject.SocketIoClientDotNet.Client;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SpawnPlayerData {

    public string username;
    public float[] position;
    public string animation;
    public PlayerStats stats;
    public CustomizationJSON customization;

}

public class NetworkedPlayerManager : MonoBehaviour {

    public GameObject networkedPlayerPrefab;

    private Socket socket;
    private List<GameObject> networkedPlayers = new List<GameObject>();

    void Start() {
        this.socket = GameObject.FindGameObjectWithTag("ServerConnection").GetComponent<SocketHandler>().getSocket();
        Debug.Log("Loading other connected players...");
    }

    public GameObject AddPlayer(string username, Vector2 position) {
        GameObject npo = Instantiate(networkedPlayerPrefab, position, Quaternion.identity, this.transform);
        networkedPlayers.Add(npo);
        return npo;
    }

}
