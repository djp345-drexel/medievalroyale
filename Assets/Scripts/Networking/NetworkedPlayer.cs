﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class NetworkedPlayer : MonoBehaviour {

    public TextMeshProUGUI nameText;
    public TextMeshProUGUI chatText;

    public Color staffNameColor;

    private PlayerStats stats;

    private string username = "unknown";

    public void SetUsername(string username)
    {
        this.username = username;
        nameText.text = username;
    }

    public void SetStats(PlayerStats stats)
    {
        this.stats = stats;
        if (bool.Parse(stats.isstaff))
        {
            nameText.color = staffNameColor;
        }
    }

    public PlayerStats GetStats()
    {
        return this.stats;
    }

    public string GetUsername()
    {
        return this.username;
    }

    public IEnumerator Chat(string chat)
    {
        chatText.text = chat;
        yield return new WaitForSeconds(7);
        if (chatText.text == chat)
        {
            chatText.text = "";
        }
    }

}
