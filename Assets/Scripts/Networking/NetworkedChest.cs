﻿using Quobject.SocketIoClientDotNet.Client;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkedChest : MonoBehaviour {

    public Sprite openSprite;
    private int id;

    private bool openned = false;

    public void SetID(int id)
    {
        this.id = id;
    }

    public int GetID()
    {
        return this.id;
    }

	private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            // Send Check to the server
            if (!this.openned)
                OpenChest();
        }
    }

    public void OpenChest()
    {
        SetOpenned();
        if (GameObject.FindGameObjectWithTag("ServerConnection") != null)
        {
            Socket socket = GameObject.FindGameObjectWithTag("ServerConnection").GetComponent<SocketHandler>().getSocket();
            socket.Emit("openChest", this.id);
        }
    }

    public void SetOpenned()
    {
        this.openned = true;
        this.GetComponent<SpriteRenderer>().sprite = openSprite;
    }

}
