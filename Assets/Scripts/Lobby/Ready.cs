﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using TMPro;
using UnityEngine.UI;

public class Ready : MonoBehaviour {

    public string webServer;
    public TextMeshProUGUI userNameText;

    private SocketHandler socketHandler;

    void Start()
    {
        GameObject serverConnection = GameObject.FindGameObjectWithTag("ServerConnection");
        if (serverConnection != null) {
            this.socketHandler = serverConnection.GetComponent<SocketHandler>();
        }
    }

    void Update()
    {
        if (socketHandler == null) return;
        if (userNameText.text != this.socketHandler.username)
        {
            userNameText.text = this.socketHandler.username;
        }
    }

    public void OnReady() {
        StartCoroutine(FindServer());
    }

    IEnumerator FindServer() {
        WWWForm form = new WWWForm();
        var response = UnityWebRequest.Post(webServer, form);
        yield return response.SendWebRequest();
        string resp = response.downloadHandler.text;
        Server gameServer = JsonUtility.FromJson<Server>(resp);
        GameObject serverConnection = GameObject.FindGameObjectWithTag("ServerConnection");
        serverConnection.GetComponent<SocketHandler>().ConnectToServer(gameServer);
    }

}
