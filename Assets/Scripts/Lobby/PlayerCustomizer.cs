﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerCustomizer : MonoBehaviour {

    public Dropdown partDropdown;
    public PlayerColorizer colorizer;

    public Slider red;
    public Slider green;
    public Slider blue;

    private SwapIndex selectedPart;
    private bool changing = false;

    private SocketHandler socketHandler;

    void Start()
    {
        ChangePart();
        GameObject serverConnection = GameObject.FindGameObjectWithTag("ServerConnection");
        if (serverConnection != null)
        {
            socketHandler = serverConnection.GetComponent<SocketHandler>();
        }
        LoadSavedCustomization();
    }

    public void ChangeColor()
    {
        if (changing) return;

        colorizer.SwapColor(selectedPart, new Color(red.value / 255, green.value / 255, blue.value / 255));
    }

    public void LoadSavedCustomization()
    {
        if (socketHandler == null) return;
        colorizer.LoadPlayerColors();
    }

    public void SaveCustomization()
    {
        CustomizationJSON newCustomizationJSON = new CustomizationJSON();
        newCustomizationJSON.skin = CreateColorString(colorizer.skin);
        newCustomizationJSON.shoes = CreateColorString(colorizer.shoes);
        newCustomizationJSON.shirt = CreateColorString(colorizer.shirt);
        newCustomizationJSON.belt = CreateColorString(colorizer.belt);
        newCustomizationJSON.hat = CreateColorString(colorizer.hat);
        newCustomizationJSON.hair = CreateColorString(colorizer.hair);
        newCustomizationJSON.sleeves = CreateColorString(colorizer.sleeves);
        newCustomizationJSON.secondary = CreateColorString(colorizer.secondary);
        socketHandler.customization = newCustomizationJSON;

        socketHandler.UpdateCustomization(new string[]{
            newCustomizationJSON.skin,
            newCustomizationJSON.shoes,
            newCustomizationJSON.shirt,
            newCustomizationJSON.belt,
            newCustomizationJSON.hat,
            newCustomizationJSON.hair,
            newCustomizationJSON.sleeves,
            newCustomizationJSON.secondary
        });
    }

    public Color ParseColor(string color)
    {
        string[] colorStringParts = color.Split(':');
        int red = int.Parse(colorStringParts[0]);
        int green = int.Parse(colorStringParts[1]);
        int blue = int.Parse(colorStringParts[2]);
        return new Color((float) red / 255, (float) green / 255, (float) blue / 255);
    }

    public String CreateColorString(Color color)
    {
        return (int)(color.r * 255) + ":" + (int)(color.g * 255) + ":" + (int)(color.b * 255);
    }

    public void ChangePart()
    {
        string part = partDropdown.options[partDropdown.value].text.ToUpper();
        this.selectedPart = (SwapIndex) Enum.Parse(typeof(SwapIndex), part);
        changing = true;

        switch (part)
        {
            case "SKIN":
                red.value = (int) (colorizer.skin.r * 255);
                green.value = (int) (colorizer.skin.g * 255);
                blue.value = (int) (colorizer.skin.b * 255);
                break;
            case "SHOES":
                red.value = (int) (colorizer.shoes.r * 255);
                green.value = (int) (colorizer.shoes.g * 255);
                blue.value = (int) (colorizer.shoes.b * 255);
                break;
            case "SHIRT":
                red.value = (int) (colorizer.shirt.r * 255);
                green.value = (int) (colorizer.shirt.g * 255);
                blue.value = (int) (colorizer.shirt.b * 255);
                break;
            case "BELT":
                red.value = (int) (colorizer.belt.r * 255);
                green.value = (int) (colorizer.belt.g * 255);
                blue.value = (int) (colorizer.belt.b * 255);
                break;
            case "HAT":
                red.value = (int) (colorizer.hat.r * 255);
                green.value = (int) (colorizer.hat.g * 255);
                blue.value = (int) (colorizer.hat.b * 255);
                break;
            case "HAIR":
                red.value = (int) (colorizer.hair.r * 255);
                green.value = (int) (colorizer.hair.g * 255);
                blue.value = (int) (colorizer.hair.b * 255);
                break;
            case "SLEEVES":
                red.value = (int) (colorizer.sleeves.r * 255);
                green.value = (int) (colorizer.sleeves.g * 255);
                blue.value = (int) (colorizer.sleeves.b * 255);
                break;
            case "SECONDARY":
                red.value = (int) (colorizer.secondary.r * 255);
                green.value = (int) (colorizer.secondary.g * 255);
                blue.value = (int) (colorizer.secondary.b * 255);
                break;
        }

        changing = false;

    }

}
