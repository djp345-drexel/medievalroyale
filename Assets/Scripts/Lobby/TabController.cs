﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TabController : MonoBehaviour
{
    public GameObject current;
    public GameObject target;

    public void GoToTab()
    {
        current.SetActive(false);
        target.SetActive(true);
    }

}
