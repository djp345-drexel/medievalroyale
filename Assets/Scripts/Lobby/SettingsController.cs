﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingsController : MonoBehaviour {

    public GameObject settings;

    private Animator animator;

    private void Awake()
    {
        this.animator = settings.GetComponent<Animator>();
    }

    public void ToggleSettingsDisplay()
    {
        animator.SetBool("IsOpen", !animator.GetBool("IsOpen"));
    }

}
