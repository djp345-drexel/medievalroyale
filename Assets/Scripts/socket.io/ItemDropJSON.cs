﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ItemDropJSON {

    public int item;
    public int amount;
    public float[] position;
    public int instance;
    
}
