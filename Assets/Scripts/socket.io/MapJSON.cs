﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MapJSON {

    public ChestJSON[] chests;
    public FloorJSON[] floors;

}

[System.Serializable]
public class ChestJSON
{
    public int id;
    public float[] position;

}

[System.Serializable]
public class FloorJSON
{

    public float x;
    public float y;

}