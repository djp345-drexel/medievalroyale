﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class StormJSON {

    public float[] stormCenter;
    public int stormState;
    public float stormSize;
    public int stormTime;

}

public enum StormState
{
    WAITING = 0,
    SHRINKING = 1,
    COMPLETE = 2,
    DISABLED = 3
}
