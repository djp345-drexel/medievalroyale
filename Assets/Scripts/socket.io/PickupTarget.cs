﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PickupTarget {

    public string targetUsername;
    public int instance;

}
