﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Server {

    public string ip;
    public int port;
    public int players;
    public bool joinable;

    private SocketHandler socketHandler;

    public Server(GameObject serverConnection, string ip, int port) {
        this.ip = ip;
        this.port = port;
        this.socketHandler = serverConnection.GetComponent<SocketHandler>();
    }

}
