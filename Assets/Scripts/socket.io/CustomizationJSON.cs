﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CustomizationJSON
{
    public string username;
    public string skin;
    public string shoes;
    public string shirt;
    public string belt;
    public string hat;
    public string hair;
    public string sleeves;
    public string secondary;

}
