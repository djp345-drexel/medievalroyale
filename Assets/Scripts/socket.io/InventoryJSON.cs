﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class InventoryJSON {

    public int[] s1;
    public int[] s2;
    public int[] s3;
    public int[] s4;
    public int[] s5;

    public int active;
	
}
