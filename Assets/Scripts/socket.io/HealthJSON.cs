﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class HealthJSON {

    public float hp;
    public float armor;
	
}
