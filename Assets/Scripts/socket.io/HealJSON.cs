﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class HealJSON {

    public float health;
    public float armor;
    public float healthCap;
    public float armorCap;

}
