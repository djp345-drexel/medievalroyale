﻿using System.Collections;
using System.Collections.Generic;
using Quobject.SocketIoClientDotNet.Client;
using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.UI;
using TMPro;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(BoxCollider2D))]

public class PlayerMotor : MonoBehaviour
{
    // Player Components
    private Rigidbody2D rigidBody;
    private Animator anim;
    private BoxCollider2D boxCollider;
    public GameObject minimap;

    [HideInInspector]
    public bool disabled = false;

    // Player Movement Variables
    private Vector2 forceDelta;
    public float speed;
    private Vector2 PrevDirection;
    private string lastAnimation;
    private bool IsMoving;
    private Vector2 lastPosition;

    // Storm Components
    public GameObject storm;
    private BoxCollider2D stormCol;

    // Storm Damage Variables
    public GameObject circle;
    private CircleCollider2D circleCol;
    private float dmgTimer = 1;

    // Attack Components
    public GameObject attackObj;
    private AttackBox attackComp;
    private BoxCollider2D attackBox;

    public Slider consumableBar;
    public TextMeshProUGUI consumableText;

    void Awake()
    {
        anim = GetComponent<Animator>();
        rigidBody = GetComponent<Rigidbody2D>();
        boxCollider = GetComponent<BoxCollider2D>();
        stormCol = storm.GetComponent<BoxCollider2D>();
        circleCol = circle.GetComponent<CircleCollider2D>();
        attackBox = attackObj.GetComponent<BoxCollider2D>();
        attackComp = attackObj.GetComponent<AttackBox>();

        lastAnimation = anim.GetCurrentAnimatorClipInfo(0)[0].clip.name;
        SendAnimationToServer(lastAnimation);
    }

    void Start()
    {
        rigidBody.gravityScale = 0;
        rigidBody.constraints = RigidbodyConstraints2D.FreezeRotation;
    }

    void Update()
    {
        CheckInput();
    }

    void FixedUpdate()
    {
        Vector2 currentPosition = transform.position;
        if (currentPosition != lastPosition)
        {
            Vector2 delta = currentPosition - lastPosition;
            SendMovementToServer(delta);
            lastPosition = currentPosition;
        }
        string currentAnimation = anim.GetCurrentAnimatorClipInfo(0)[0].clip.name;
        if (currentAnimation != lastAnimation)
        {
            //Debug.Log(currentAnimation);
            SendAnimationToServer(currentAnimation);
            lastAnimation = currentAnimation;
        }
    }

    void CheckInput()
    {
        if (this.disabled)
            return;

        IsMoving = false;

        if (Input.GetKeyDown(KeyCode.M))
        {
            minimap.SetActive(!minimap.activeInHierarchy);
        }

        var horiz = Input.GetAxisRaw("Horizontal");
        var vert = Input.GetAxisRaw("Vertical");

        if (horiz < 0 || horiz > 0 || vert < 0 || vert > 0)
        {
            IsMoving = true;

            if (rigidBody.velocity.x != 0 || rigidBody.velocity.y != 0)
            {
                PrevDirection = rigidBody.velocity;
            }

        }

        forceDelta = new Vector2(horiz, vert);
        CalculateMovement(forceDelta * speed);

        // If Attacking
        if (Input.GetKeyDown(KeyCode.S))
        {
            GameObject invObject = GameObject.Find("Item Bar");
            Inventory inventory = invObject.GetComponent<Inventory>();
            Item currentItem = inventory.GetCurrentItem();

            if (currentItem != null && currentItem.GetType() == typeof(Consumable))
            {
                StartCoroutine(UseConsumable((Consumable) currentItem));
                return;
            }

            // If facing Left
            if (PrevDirection.x < 0)
            {
                // Change the attack box to face the left and scale it to match the size in terms of the direction he is facing
                attackObj.transform.localPosition = new Vector3(-1, 0, 0);
                attackObj.transform.localScale = new Vector3(1, 1, 1);
            }
            // If facing Right
            else if (PrevDirection.x > 0)
            {
                // Change the attack box to face the right and scale it to match the size in terms of the direction he is facing
                attackObj.transform.localPosition = new Vector3(1, 0, 0);
                attackObj.transform.localScale = new Vector3(1, 1, 1);
            }
            // If facing Down
            else if (PrevDirection.y < 0)
            {
                // Change the attack box to face the down and scale it to match the size in terms of the direction he is facing
                attackObj.transform.localPosition = new Vector3(0, -1, 0);
                attackObj.transform.localScale = new Vector3(1, 1, 1);
            }
            // If facing Up
            else if (PrevDirection.y > 0)
            {
                // Change the attack box to face the Up and scale it to match the size in terms of the direction he is facing
                attackObj.transform.localPosition = new Vector3(0, 1, 0);
                attackObj.transform.localScale = new Vector3(1, 1, 1);
            }
            // Turn on the collider for the attack hit box
            StartCoroutine(Attack());
        }
    }

    void CalculateMovement(Vector2 forcePlayer)
    {
        rigidBody.velocity = Vector2.zero;
        rigidBody.AddForce(forcePlayer, ForceMode2D.Impulse);
        SendAnimInfo();
    }

    void SendAnimInfo()
    {
        anim.SetFloat("XSpeed", rigidBody.velocity.x);
        anim.SetFloat("YSpeed", rigidBody.velocity.y);
        anim.SetFloat("PrevX", PrevDirection.x);
        anim.SetFloat("PrevY", PrevDirection.y);
        anim.SetBool("IsMoving", IsMoving);
    }

    void SendAnimationToServer(string animation)
    {
        if (!IsOffline())
        {
            Socket socket = GameObject.FindGameObjectWithTag("ServerConnection").GetComponent<SocketHandler>().getSocket();
            if (socket != null)
                socket.Emit("animation", animation);
        }
    }

    void SendMovementToServer(Vector2 d)
    {
        if (!IsOffline())
        {
            GameObject.FindGameObjectWithTag("ServerConnection").GetComponent<SocketHandler>().getSocket().Emit("move", d.x, d.y);
        }
    }

    bool IsOffline()
    {
        return GameObject.FindGameObjectWithTag("ServerConnection") == null;
    }

/*    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "Health")
        {
            health.Heal(25, HealthType.HEALTH);
        }
        else if (collision.gameObject.name == "Damage")
        {
            health.Hurt(15);
        }
        else if (collision.gameObject.name == "Armor")
        {
            health.Heal(20, HealthType.ARMOR);
        }
    }*/

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.name == "Storm Circle")
        {
            Debug.Log("NO DAMAGE");
            stormCol.enabled = false;
        }
        else if (collision.gameObject.name == "Storm")
        {
            dmgTimer -= Time.deltaTime;
            Debug.Log("DAMAGE");
            if (dmgTimer <= 0)
            {
                if (!IsOffline())
                {
                    GameObject.FindGameObjectWithTag("ServerConnection").GetComponent<SocketHandler>().getSocket().Emit("stormDamage");
                }
                dmgTimer = 1;
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.name == "Storm Circle")
        {
            stormCol.enabled = true;
        }
    }

    public IEnumerator UseConsumable(Consumable consumable)
    {
        float t = 0;
        consumableBar.gameObject.SetActive(true);
        this.disabled = true;
        while (t <= consumable.duration)
        {
            consumableBar.value = t / consumable.duration;
            consumableText.text = Mathf.Ceil(consumable.duration - t) + " secs left...";
            t += .02F;
            yield return new WaitForSeconds(.02F);
        }
        if (!IsOffline())
        {
            HealJSON healJSON = new HealJSON();
            healJSON.health = consumable.health;
            healJSON.healthCap = consumable.healthCap;
            healJSON.armor = consumable.armor;
            healJSON.armorCap = consumable.armorCap;
            GameObject.FindGameObjectWithTag("ServerConnection").GetComponent<SocketHandler>().getSocket().Emit("consumable", JsonUtility.ToJson(healJSON));
        }
        consumableBar.value = 0;
        consumableText.text = "";
        consumableBar.gameObject.SetActive(false);
        this.disabled = false;
    }

    public IEnumerator Attack()
    {
        float freeze = .417F / 2F;

        GameObject inventoryObject = GameObject.Find("Item Bar");
        Inventory inventory = inventoryObject.GetComponent<Inventory>();
        Item item = inventory.GetCurrentItem();

        if (item != null)
        {
            if (item.GetType() == typeof(Weapon))
            {
                Weapon weapon = (Weapon)item;
                freeze = weapon.freeze;
            }
        }

        this.disabled = true;
        IsMoving = false;
        attackBox.enabled = true;
        rigidBody.velocity = Vector2.zero;
        anim.SetTrigger("IsAttacking");
        yield return new WaitForSeconds(freeze);
        attackBox.enabled = false;
        this.disabled = false;
    }

    public bool IsDisabled()
    {
        return this.disabled;
    }

}