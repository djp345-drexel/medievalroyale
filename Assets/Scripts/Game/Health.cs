﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Health : MonoBehaviour {

    public Slider healthBar;
    public Slider armorBar;
    public TextMeshProUGUI healthText;
    public TextMeshProUGUI armorText;

    private float health = 100;
    private float armor = 0;

    private void Start()
    {
        UpdateHealthBars(100, 0);
    }

    public void UpdateHealth(HealthJSON json)
    {
        //Debug.Log("New Health: " + json.hp + " || New Armor: " + json.armor);
        this.health = json.hp;
        this.armor = json.armor;
        UpdateHealthBars(json.hp, json.armor);
    }

    private void UpdateHealthBars(float health, float armor)
    {
        healthBar.value = health / 100;
        armorBar.value = armor / 100;
        healthText.text = "" + health;
        armorText.text = "" + armor;
    }

}
