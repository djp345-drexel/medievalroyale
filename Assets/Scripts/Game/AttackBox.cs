﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackBox : MonoBehaviour {

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "NetworkedPlayer") //successfully hit target
        {
            float damage = 5;
            GameObject inventoryObject = GameObject.Find("Item Bar");
            Inventory inventory = inventoryObject.GetComponent<Inventory>();
            Item item = inventory.GetCurrentItem();

            if (item != null)
            {
                if (item.GetType() == typeof(Weapon))
                {
                    Weapon weapon = (Weapon) item;
                    damage = weapon.damage;
                }
            }

            NetworkedPlayer npTarget = collision.gameObject.GetComponent<NetworkedPlayer>();
            if (!IsOffline())
                GameObject.FindGameObjectWithTag("ServerConnection").GetComponent<SocketHandler>().getSocket().Emit("hit", npTarget.GetUsername(), damage);
            this.GetComponent<Collider2D>().enabled = false;
        }
    }

    private bool IsOffline()
    {
        return GameObject.FindGameObjectWithTag("ServerConnection") == null;
    }

}
