﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Quobject.SocketIoClientDotNet.Client;
using UnityEngine.UI;
using TMPro;

public class ChatManager : MonoBehaviour {

    public TextMeshProUGUI playerChat;

    public InputField chatInput;

    private List<string> previousChats = new List<string>();
    private int barIndex = 0;

	void Update () {
		if (Input.GetKeyDown(KeyCode.Tab))
        {
            chatInput.gameObject.SetActive(!chatInput.gameObject.activeInHierarchy);
            if (chatInput.gameObject.activeInHierarchy)
            {
                chatInput.Select();
                
                GameObject.Find("Player").GetComponent<PlayerMotor>().disabled = true;
            } else
                GameObject.Find("Player").GetComponent<PlayerMotor>().disabled = false;
        }
        if (!chatInput.gameObject.activeInHierarchy) return;
        if (Input.GetKeyDown(KeyCode.Return))
        {
            string chat = chatInput.text.Trim();
            chatInput.text = "";
            if (chat != "" && (previousChats.Count == 0 || chat != previousChats[previousChats.Count - 1]))
            {
                previousChats.Add(chat);
                barIndex = previousChats.Count - 1;
            }
            chatInput.Select();
            StartCoroutine(UpdateChat(chat));
            SendChatToServer(chat);
        }

        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            if (previousChats.Count == 0) return;
            if (barIndex >= 0)
            {
                chatInput.text = previousChats[barIndex];
                if (barIndex != 0)
                    barIndex--;
            }
        }

        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            if (barIndex < previousChats.Count - 1)
                chatInput.text = previousChats[++barIndex];
            else
                chatInput.text = "";
        }

	}

    IEnumerator UpdateChat(string chat)
    {
        playerChat.text = chat;
        yield return new WaitForSeconds(7);
        if (playerChat.text == chat)
        {
            playerChat.text = "";
        }
    }

    void SendChatToServer(string chat)
    {
        Socket socket = GameObject.FindGameObjectWithTag("ServerConnection").GetComponent<SocketHandler>().getSocket();
        if (socket != null)
            socket.Emit("chat", chat);
    }

}
