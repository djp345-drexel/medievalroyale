﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class World : MonoBehaviour {

    public Item[] items;

    public GameObject networkedChest;
    public GameObject itemDrop;

    private List<GameObject> chests = new List<GameObject>();
    private List<GameObject> itemDrops = new List<GameObject>();

	public void AddChest(ChestJSON chestJSON)
    {
        GameObject chest = Instantiate(networkedChest, new Vector2(chestJSON.position[0], chestJSON.position[1]), Quaternion.identity);
        NetworkedChest nChest = chest.GetComponent<NetworkedChest>();
        nChest.SetID(chestJSON.id);
        chests.Add(chest);
    }

    public void AddItemDrop(ItemDropJSON itemDropJSON)
    {
        //Debug.Log("Dropping item: " + itemDropJSON.item + " with ID: " + itemDropJSON.instance);
        Item item = FindItemByID(itemDropJSON.item);
        GameObject ido = Instantiate(itemDrop, new Vector2(itemDropJSON.position[0], itemDropJSON.position[1]), Quaternion.identity);
        ItemPickup itemPickup = ido.GetComponentInChildren<ItemPickup>();
        itemPickup.SetItem(item, itemDropJSON.instance);
        itemDrops.Add(ido);
    }

    public NetworkedChest FindChestByID(int chestID)
    {
        foreach (GameObject chest in chests)
        {
            NetworkedChest nChest = chest.GetComponent<NetworkedChest>();
            if (nChest.GetID() == chestID)
                return nChest;
        }
        return null;
    }

    public Item FindItemByID(int itemID)
    {
        foreach (Item item in items)
        {
            if (item.itemId == itemID)
                return item;
        }
        return null;
    }

    public ItemPickup FindItemDropByInstance(int instance)
    {
        foreach (GameObject itemDropObject in itemDrops)
        {
            ItemPickup pickup = itemDropObject.GetComponentInChildren<ItemPickup>();
            if (pickup == null) continue;
            //Debug.Log("Trying to find pickup: " + instance + " - TESTING: " + pickup.GetItemInstanceID());
            if (instance == pickup.GetItemInstanceID())
                return pickup;
        }
        return null;
    }

    public void RemoveItem(int instance)
    {
        GameObject toRemove = null;
        foreach (GameObject itemDropObject in itemDrops)
        {
            ItemPickup pickup = itemDropObject.GetComponentInChildren<ItemPickup>();
            if (instance == pickup.GetItemInstanceID())
            {
                toRemove = itemDropObject;
                break;
            }
        }
        if (toRemove != null)
            itemDrops.Remove(toRemove);
        Destroy(toRemove);
    }

}
