﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Storm : MonoBehaviour {

    public Transform nextCircle;
    public Transform stormMask;

    public TextMeshProUGUI uiText;

    private StormJSON json;
    private Vector3 lastScale;

    public void UpdateStorm(StormJSON stormJSON)
    {
        this.json = stormJSON;
        switch(stormJSON.stormState)
        {
            case 0: // Waiting
                nextCircle.position = new Vector3(stormJSON.stormCenter[0], stormJSON.stormCenter[1], 1);
                nextCircle.localScale = new Vector3(stormJSON.stormSize, stormJSON.stormSize, 1);
                this.lastScale = stormMask.localScale;
                break;
            case 1: // Shrinking
                StartCoroutine(ShrinkStorm(stormJSON.stormTime));
                break;
        }
        StartCoroutine(UpdateTimer(stormJSON.stormState, stormJSON.stormTime));
    }
    
    public IEnumerator UpdateTimer(int state, int time)
    {
        int t = 0;
        string text = "";
        switch (state)
        {
            case 0:
                text = "Waiting Phase\n";
                break;
            case 1:
                text = "Shrinking Phase\n";
                break;
            case 2:
                text = "Storm Complete";
                break;
            case 3:
                text = "Storm Disabled";
                break;
        }
        if (state == 2 || state == 3)
        {
            uiText.text = text;
            yield break;
        }
        while (t < time)
        {
            uiText.text = text + "" + (time - t) + " seconds left";
            t += 1;
            yield return new WaitForSeconds(1);
        }
    }
    
    public IEnumerator ShrinkStorm(float time)
    {
        stormMask.position = nextCircle.position;
        Vector3 final = nextCircle.localScale;
        float timePassed = 0;
        while (timePassed < time)
        {
            Vector3 updatedScale = Vector3.Lerp(lastScale, final, timePassed / time);
            stormMask.localScale = updatedScale;
            timePassed += Time.deltaTime;
            yield return null;
        }
        stormMask.localScale = final;
    }

}