﻿using Quobject.SocketIoClientDotNet.Client;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemPickup : MonoBehaviour {

    private Item item;

    private float iterations = 30;
    private float time = 0.5F;

    private int instance_id;
    private bool pickingUp;

    private new CircleCollider2D collider = null;
    private new Animation animation = null;

    private Inventory inventory;

    private void Start()
    {
        this.collider = GetComponent<CircleCollider2D>();
        this.animation = GetComponent<Animation>();
        this.inventory = GameObject.Find("Item Bar").GetComponent<Inventory>();
    }

    public void SetItem(Item item, int instanceID)
    {
        this.item = item;
        this.instance_id = instanceID;
        this.GetComponent<SpriteRenderer>().sprite = item.GetIcon();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (pickingUp) return;
        if (inventory.IsFull()) return;
        if (collision.gameObject.tag == "Player")
        {
            Destroy(gameObject.GetComponent<CircleCollider2D>());
            Destroy(gameObject.GetComponent<Animation>());
            if (GameObject.FindGameObjectWithTag("ServerConnection") != null)
            {
                Socket socket = GameObject.FindGameObjectWithTag("ServerConnection").GetComponent<SocketHandler>().getSocket();
                socket.Emit("pickupTarget", this.instance_id);
            }
            StartCoroutine(Pickup(collision.gameObject));
        }
    }

    public IEnumerator Pickup(GameObject target)
    {
        pickingUp = true;
        float t = 0;
        yield return new WaitForSeconds(.5F);
        while (Vector2.Distance(transform.position, target.transform.position) > .5F)
        {
            transform.position = Vector2.Lerp(transform.position, target.transform.position, t);
            t += 1 / iterations;
            yield return new WaitForSeconds(1 / (iterations / time));
        }
        if (target.tag == "Player")
        {
            if (GameObject.FindGameObjectWithTag("ServerConnection") != null)
            {
                Socket socket = GameObject.FindGameObjectWithTag("ServerConnection").GetComponent<SocketHandler>().getSocket();
                socket.Emit("pickup", this.instance_id);
            }
        }
        GameObject worldObject = GameObject.Find("World");
        World world = worldObject.GetComponent<World>();
        world.RemoveItem(this.instance_id);
        //Debug.Log("Destroyed!");
    }

    public int GetItemInstanceID()
    {
        return this.instance_id;
    }

}
