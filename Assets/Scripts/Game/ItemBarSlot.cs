﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System;

[RequireComponent(typeof(Button))]
public class ItemBarSlot : MonoBehaviour
{
    //private string key;
    public bool slotActive = false;
    public Button slot1 { get; private set; }
    Graphic targetGraphic;
    Color normalColor;
    public KeyCode key;
    public int slotIndex;

    private Item item = null;

    void Awake()
    {
        slot1 = GetComponent<Button>();
        slot1.interactable = false;
        targetGraphic = GetComponent<Graphic>();

        ColorBlock cb = slot1.colors;
        cb.disabledColor = cb.normalColor;
        slot1.colors = cb;
    }

    void Start()
    {
        slot1.targetGraphic = null;
        //List<string> keys = new List<string>(new string[] { "Item Slot 1", "Item Slot 2", "Item Slot 3", "Item Slot 4", "Item Slot 5" });
        int slot_num = Int32.Parse(gameObject.name.Replace("Slot ", "")) - 1;
    }

    void Update()
    {
        List<string> slots = new List<string>(new string[] { "Slot 1", "Slot 2", "Slot 3", "Slot 4", "Slot 5" });
        slots.Remove(gameObject.name);

        for (int i = 0; i < slots.Count; i += 1)
        {
            GameObject other_slot = GameObject.Find(slots[i]);
            ItemBarSlot slot2 = other_slot.GetComponent<ItemBarSlot>();
            Button slot2_button = slot2.GetComponent<Button>();
            bool slot2_active = slot2.slotActive;
            Graphic targetGraphic2;
            targetGraphic2 = slot2.GetComponent<Graphic>();

            if (Input.GetKeyDown(key))
            {
                if (GameObject.Find("Player").GetComponent<PlayerMotor>().IsDisabled())
                    return;
                ChangeSlotImage(targetGraphic, slot1.colors.pressedColor, false);
                slot1.onClick.Invoke();
                slotActive = true;

                if (slot2_active)
                {
                    slot2.slotActive = false;
                    ChangeSlotImage(targetGraphic2, slot2_button.colors.normalColor, false);
                }
            }
        }
    }

    void ChangeSlotImage(Graphic slotImage, Color targetColor, bool instant)
    {
        if (slotImage == null)
            return;

        slotImage.CrossFadeColor(targetColor, instant ? 0f : slot1.colors.fadeDuration, true, true);
    }

    public void SetItem(Item item)
    {
        this.item = item;
    }

    public Item GetItem()
    {
        return this.item;
    }

}