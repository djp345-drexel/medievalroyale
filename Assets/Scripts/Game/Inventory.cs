﻿using Quobject.SocketIoClientDotNet.Client;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Inventory : MonoBehaviour {

    public ItemBarSlot[] slots;

    private int activeSlotIndex = 0;

    private void Update()
    {
        int active = FindActiveSlot();
        if (activeSlotIndex != active)
        {
            activeSlotIndex = active;
            if (GameObject.FindGameObjectWithTag("ServerConnection") != null)
            {
                Socket socket = GameObject.FindGameObjectWithTag("ServerConnection").GetComponent<SocketHandler>().getSocket();
                socket.Emit("changeSlot", this.activeSlotIndex);
            }
        }
        if (Input.GetKeyDown(KeyCode.Q))
        {
            if (GameObject.Find("Player").GetComponent<PlayerMotor>().IsDisabled())
                return;
            Item item = slots[this.activeSlotIndex].GetItem();
            if (item != null)
            {
                if (GameObject.FindGameObjectWithTag("ServerConnection") != null)
                {
                    Socket socket = GameObject.FindGameObjectWithTag("ServerConnection").GetComponent<SocketHandler>().getSocket();
                    socket.Emit("drop");
                }
            }
        }
    }

    private int FindActiveSlot()
    {
        for (int i = 0; i < slots.Length; i ++)
        {
            if (slots[i].slotActive)
                return i;
        }
        return -1;
    }

    public void SetActiveSlot(int slotIndex)
    {
        foreach (ItemBarSlot slot in slots)
        {
            slot.slotActive = false;
        }
        slots[slotIndex].slotActive = true;
    }

    public void UpdateInventory(InventoryJSON json)
    {
        UpdateSlot(0, json.s1[0], json.s1[1]);
        UpdateSlot(1, json.s2[0], json.s2[1]);
        UpdateSlot(2, json.s3[0], json.s3[1]);
        UpdateSlot(3, json.s4[0], json.s4[1]);
        UpdateSlot(4, json.s5[0], json.s5[1]);
        SetActiveSlot(json.active);
    }

    private void UpdateSlot(int slot, int itemID, int amount)
    {
        GameObject worldObject = GameObject.Find("World");
        World world = worldObject.GetComponent<World>();
        if (itemID != 0)
        {
            Item item = world.FindItemByID(itemID);
            slots[slot].gameObject.transform.GetChild(0).GetComponent<Image>().sprite = item.icon;
            slots[slot].gameObject.transform.GetChild(0).GetComponent<RectTransform>().sizeDelta = new Vector2(16, 16);
            slots[slot].gameObject.transform.GetChild(0).GetComponent<Image>().color = new Color(1, 1, 1, 1);
            slots[slot].SetItem(item);
        }
        else
        {
            slots[slot].gameObject.transform.GetChild(0).GetComponent<Image>().color = new Color(1, 1, 1, 0);
            slots[slot].SetItem(null);
        }
    }

    public Item GetCurrentItem()
    {
        return slots[activeSlotIndex].GetItem();
    }

    public bool IsFull()
    {
        foreach (ItemBarSlot slot in slots)
        {
            if (slot.GetItem() == null)
                return false;
        }
        return true;
    }

}
