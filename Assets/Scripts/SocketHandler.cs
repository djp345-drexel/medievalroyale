﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Quobject.SocketIoClientDotNet.Client;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

/*
 *  Written by Dylan Paikin
 *  Client for connection to one of the gameservers
 * 
 */

public class SocketHandler : MonoBehaviour {

    public int gameSceneIndex;
    public GameObject networkedPlayerPrefab;

    private Socket socket;

    public string email;
    public string username;

    public CustomizationJSON customization;

    private List<GameObject> networkedPlayers = new List<GameObject>();

    void Awake() {
        DontDestroyOnLoad(transform.gameObject);
    }

    void OnApplicationQuit() {
        OnDestroy();
    }

    void OnDestroy() {
        if (socket != null) {
            socket.Emit("disconnect");
            socket.Disconnect();
        }
    }

    public void Connect(string email) {
        this.email = email;
        StartCoroutine(UpdateUsername(email));
    }

    IEnumerator UpdateUsername(string email) {
        WWWForm form = new WWWForm();
        form.AddField("email", email);
        var response = UnityWebRequest.Post("http://192.241.150.199:3000/auth/find", form);
        yield return response.SendWebRequest();
        string user = response.downloadHandler.text;
        this.username = user;
        StartCoroutine(GetCustomization(username));
    }

    IEnumerator GetCustomization(string username) {
        WWWForm form = new WWWForm();
        form.AddField("username", username);
        var response = UnityWebRequest.Post("http://192.241.150.199:3000/customization/get", form);
        yield return response.SendWebRequest();
        string customization = response.downloadHandler.text;
        this.customization = JsonUtility.FromJson<CustomizationJSON>(customization);
    }

    public void UpdateCustomization(string[] colors)
    {
        WWWForm form = new WWWForm();
        form.AddField("email", email);

        form.AddField("skin", colors[0]);
        form.AddField("shoes", colors[1]);
        form.AddField("shirt", colors[2]);
        form.AddField("belt", colors[3]);
        form.AddField("hat", colors[4]);
        form.AddField("hair", colors[5]);
        form.AddField("sleeves", colors[6]);
        form.AddField("secondary", colors[7]);

        var req = UnityWebRequest.Post("http://192.241.150.199:3000/customization/update", form);
        req.SendWebRequest();
    }

    public void ConnectToServer(Server gameServer) {
        SceneManager.LoadScene(gameSceneIndex);
        Debug.Log("Connecting to gameserver @" + gameServer.ip + ":" + gameServer.port);
        socket = IO.Socket("http://" + gameServer.ip + ":" + gameServer.port);

        socket.On(Socket.EVENT_CONNECT, () => {
            socket.Emit("setup", this.username);
        });

        socket.On("registerPlayer", (data) => {
            Debug.Log("Register player data: " + data);
            SpawnPlayerData spd = JsonUtility.FromJson<SpawnPlayerData>(data.ToString());
            Vector2 pos = new Vector2(spd.position[0], spd.position[1]);
            UnityMainThreadDispatcher.Instance().Enqueue(AddPlayer(spd.username, pos, spd.animation, spd.stats, spd.customization));
        });

        socket.On("move", (data) => {
            Movement movement = JsonUtility.FromJson<Movement>(data.ToString());
            UnityMainThreadDispatcher.Instance().Enqueue(UpdatePlayerPosition(movement.username, new Vector2(movement.x, movement.y)));
        });

        socket.On("leave", (data) => {
            LeaveJSON json = JsonUtility.FromJson<LeaveJSON>(data.ToString());
            Debug.Log(json.username + " has left!");
            UnityMainThreadDispatcher.Instance().Enqueue(RemovePlayer(json.username));
        });

        socket.On("animation", (data) =>
        {
            AnimationJSON json = JsonUtility.FromJson<AnimationJSON>(data.ToString());
            UnityMainThreadDispatcher.Instance().Enqueue(UpdateAnimation(json.username, json.animation));
        });

        socket.On("chat", (data) =>
        {
            ChatJSON json = JsonUtility.FromJson<ChatJSON>(data.ToString());
            UnityMainThreadDispatcher.Instance().Enqueue(UpdateChat(json.username, json.chat));
        });

        socket.On("hit", (data) =>
        {
            HitJSON json = JsonUtility.FromJson<HitJSON>(data.ToString());
            UnityMainThreadDispatcher.Instance().Enqueue(HitPlayer(json.player, json.damage));
        });

        socket.On("storm", (data) =>
        {
            //Debug.Log(data);
            StormJSON json = JsonUtility.FromJson<StormJSON>(data.ToString());
            UnityMainThreadDispatcher.Instance().Enqueue(UpdateStorm(json));
        });

        socket.On("chest", (data) =>
        {
            ChestJSON json = JsonUtility.FromJson<ChestJSON>(data.ToString());
            UnityMainThreadDispatcher.Instance().Enqueue(SpawnChest(json));
        });

        socket.On("openChest", (data) =>
        {
            ChestJSON json = JsonUtility.FromJson<ChestJSON>(data.ToString());
            UnityMainThreadDispatcher.Instance().Enqueue(OpenChest(json));
        });

        socket.On("itemDrop", (data) =>
        {
            ItemDropJSON json = JsonUtility.FromJson<ItemDropJSON>(data.ToString());
            UnityMainThreadDispatcher.Instance().Enqueue(SpawnItemDrop(json));
        });

        socket.On("inventory", (data) =>
        {
            InventoryJSON json = JsonUtility.FromJson<InventoryJSON>(data.ToString());
            UnityMainThreadDispatcher.Instance().Enqueue(UpdateInventory(json));
        });

        socket.On("health", (data) =>
        {
            HealthJSON json = JsonUtility.FromJson<HealthJSON>(data.ToString());
            UnityMainThreadDispatcher.Instance().Enqueue(UpdateHealth(json));
        });

        socket.On("pickupTarget", (data) =>
        {
            PickupTarget json = JsonUtility.FromJson<PickupTarget>(data.ToString());
            UnityMainThreadDispatcher.Instance().Enqueue(UpdatePickupTarget(json));
        });

        socket.On("teleport", (data) =>
        {
            TeleportJSON json = JsonUtility.FromJson<TeleportJSON>(data.ToString());
            UnityMainThreadDispatcher.Instance().Enqueue(Teleport(json));
        });

        socket.On("dead", () =>
        {
            UnityMainThreadDispatcher.Instance().Enqueue(Die());
        });

        socket.On("win", () =>
        {
            UnityMainThreadDispatcher.Instance().Enqueue(Win());
        });

        socket.On("start", () =>
        {
            UnityMainThreadDispatcher.Instance().Enqueue(Spawn());
        });

    }

    public IEnumerator UpdatePlayerPosition(string username, Vector2 position) {
        GameObject p = GetPlayerObject(username);
        NetworkedPlayer np = p.GetComponent<NetworkedPlayer>();
        p.transform.position = position;
        yield return null;
    }

    public IEnumerator AddPlayer(string username, Vector2 position, string animation, PlayerStats stats, CustomizationJSON colors) {
        GameObject npo = Instantiate(networkedPlayerPrefab, position, Quaternion.identity, this.transform);
        npo.GetComponent<NetworkedPlayer>().SetUsername(username);
        npo.GetComponent<NetworkedPlayer>().SetStats(stats);
        //npo.GetComponent<PlayerColorizer>().CreateSwapTexture();
        npo.GetComponent<PlayerColorizer>().LoadColors(colors);
        networkedPlayers.Add(npo);
        UpdateAnimation(username, animation);
        yield return null;
    }

    public IEnumerator RemovePlayer(string username) {
        Debug.Log("Destroying " + username);
        GameObject p = GetPlayerObject(username);
        networkedPlayers.Remove(p);
        Destroy(p);
        yield return null;
    }
    
    public IEnumerator UpdateChat(string username, string chat)
    {
        GameObject target = GetPlayerObject(username);
        NetworkedPlayer np = target.GetComponent<NetworkedPlayer>();
        StartCoroutine(np.Chat(chat));
        yield return null;
    }

    public IEnumerator UpdateAnimation(string username, string animation)
    {
        GameObject target = GetPlayerObject(username);
        Animator targetAnim = target.GetComponent<Animator>();
        //Debug.Log(username + "'s animation: " + animation);
        targetAnim.Play(animation, 0);
        yield return null;
    }

    public IEnumerator HitPlayer(string username, int damage) {
        if (this.username == username) { // got hit
            SpriteRenderer sr = GameObject.Find("Player").GetComponent<SpriteRenderer>();
            sr.color = new Color(1, 0, 0);
            yield return new WaitForSeconds(0.5F);
            sr.color = new Color(1, 1, 1);
        } else { // other player got hit
            SpriteRenderer sr = GetPlayerObject(username).GetComponent<SpriteRenderer>();
            sr.color = new Color(1, 0, 0);
            yield return new WaitForSeconds(0.5F);
            sr.color = new Color(1, 1, 1);
        }
    }

    public IEnumerator UpdateStorm(StormJSON json)
    {
        //Debug.Log("Storm update packet: " + json.stormState);
        GameObject stormObject = GameObject.Find("Storm");
        if (json.stormState == (int) StormState.DISABLED)
        {
            //stormObject.SetActive(false);
        } else {
            Storm storm = stormObject.GetComponent<Storm>();
            storm.UpdateStorm(json);
        }
        yield return null;
    }

    public IEnumerator SpawnChest(ChestJSON json)
    {
        GameObject worldObject = GameObject.Find("World");
        World world = worldObject.GetComponent<World>();
        world.AddChest(json);
        yield return null;
    }

    public IEnumerator OpenChest(ChestJSON json)
    {
        GameObject worldObject = GameObject.Find("World");
        World world = worldObject.GetComponent<World>();
        world.FindChestByID(json.id).SetOpenned();
        yield return null;
    }

    public IEnumerator SpawnItemDrop(ItemDropJSON json)
    {
        if (json == null)
            yield break;
        GameObject worldObject = GameObject.Find("World");
        World world = worldObject.GetComponent<World>();
        world.AddItemDrop(json);
        yield return null;
    }

    public IEnumerator UpdateInventory(InventoryJSON json)
    {
        GameObject inventoryObject = GameObject.Find("Item Bar");
        Inventory inventory = inventoryObject.GetComponent<Inventory>();
        inventory.UpdateInventory(json);
        yield return null;
    }

    public IEnumerator UpdateHealth(HealthJSON json)
    {
        GameObject player = GameObject.Find("Player");
        Health health = player.GetComponent<Health>();
        health.UpdateHealth(json);
        yield return null;
    }

    public IEnumerator UpdatePickupTarget(PickupTarget json)
    {
        GameObject worldObject = GameObject.Find("World");
        World world = worldObject.GetComponent<World>();
        ItemPickup pickup = world.FindItemDropByInstance(json.instance);
        //Debug.Log(json.instance + " picked up by " + json.targetUsername);
        //StartCoroutine(pickup.Pickup(GetPlayerObject(json.targetUsername)));
        pickup.transform.parent.position = GetPlayerObject(json.targetUsername).transform.position;
        world.RemoveItem(json.instance);
        yield return null;
        //yield return StartCoroutine(pickup.Pickup(GetPlayerObject(json.targetUsername)));
    }

    public GameObject GetPlayerObject(string username) {
        NetworkedPlayer np = null;
        foreach (GameObject netPlayer in networkedPlayers) {
            np = netPlayer.GetComponent<NetworkedPlayer>();
            if (np.GetUsername() == username)
                return netPlayer;
        }
        return null;
    }

    public IEnumerator Teleport(TeleportJSON json)
    {
        GameObject player = GameObject.Find("Player");
        //Debug.Log("Teleport pos: " + json.position[0] + ":" + json.position[1]);
        player.transform.position = new Vector2(json.position[0], json.position[1]);
        yield return null;
    }

    public IEnumerator Die()
    {
        SceneManager.LoadScene(3);
        Destroy(this);
        yield return null;
    }

    public IEnumerator Win()
    {
        SceneManager.LoadScene(4);
        Destroy(this);
        yield return null;
    }

    public IEnumerator Spawn()
    {
        float x = 0;
        float y = 0;
        while (true)
        {
            x = UnityEngine.Random.Range(-301F, 19F);
            y = UnityEngine.Random.Range(1003F, 1300F);
            RaycastHit2D ray = Physics2D.Raycast(new Vector3(x, y, 3), -Vector2.up);
            if (ray.collider == null)
                break;
        }
        GameObject.Find("Player").transform.position = new Vector2(x, y);
        yield return null;
    }

    public Socket getSocket() {
        return socket;
    }

}
