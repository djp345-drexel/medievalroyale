﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerColorizer : MonoBehaviour {

    public bool sprite;
    public bool playerSync;

    public Color skin;
    public Color shoes;
    public Color shirt;
    public Color belt;
    public Color hat;
    public Color hair;
    public Color sleeves;
    public Color secondary;

    private CustomizationJSON lastCustomization;

    private Texture2D swapTexture;

    void Awake() {
        if (swapTexture == null)
            swapTexture = CreateSwapTexture();
    }

    void Start () {
        SwapColor(SwapIndex.SKIN, skin);
        SwapColor(SwapIndex.SHOES, shoes);
        SwapColor(SwapIndex.SHIRT, shirt);
        SwapColor(SwapIndex.BELT, belt);
        SwapColor(SwapIndex.HAT, hat);
        SwapColor(SwapIndex.HAIR, hair);
        SwapColor(SwapIndex.SLEEVES, sleeves);
        SwapColor(SwapIndex.SECONDARY, secondary);

        if (sprite)
            this.GetComponent<SpriteRenderer>().material.SetTexture("_SwapTex", swapTexture);
        else
            this.GetComponent<Image>().material.SetTexture("_SwapTex", swapTexture);

        if (playerSync)
            LoadPlayerColors();
    }

    public Texture2D CreateSwapTexture() {
        Texture2D swapTexture = new Texture2D(256, 1, TextureFormat.RGBA32, false, false);
        swapTexture.filterMode = FilterMode.Point;
        for (int i = 0; i < swapTexture.width; ++i)
            swapTexture.SetPixel(i, 0, new Color(1f, 1f, 0.0f, 0.0f));

        swapTexture.Apply();
        return swapTexture;
    }

    // OPTIMIZE THIS SOON
    void FixedUpdate()
    {
        if (playerSync)
            LoadPlayerColors();    
    }

    public void SwapColor(SwapIndex swap, Color color)
    {
        switch(swap)
        {
            case SwapIndex.SKIN:
                skin = color;
                swapTexture.SetPixel(244, 0, color);
                swapTexture.SetPixel(228, 0, color);
                swapTexture.SetPixel(188, 0, GetAddedColor(color, -56, -56, -72));
                break;
            case SwapIndex.SHOES:
                shoes = color;
                swapTexture.SetPixel(196, 0, color);
                break;
            case SwapIndex.SHIRT:
                shirt = color;
                swapTexture.SetPixel(56, 0, color);
                swapTexture.SetPixel(64, 0, GetAddedColor(color, 8, 72, 8));
                break;
            case SwapIndex.BELT:
                belt = color;
                swapTexture.SetPixel(235, 0, color);
                break;
            case SwapIndex.HAT:
                hat = color;
                swapTexture.SetPixel(80, 0, color);
                swapTexture.SetPixel(120, 0, GetAddedColor(color, 40, 40, 16));
                break;
            case SwapIndex.HAIR:
                hair = color;
                swapTexture.SetPixel(236, 0, color);
                break;
            case SwapIndex.SLEEVES:
                sleeves = color;
                swapTexture.SetPixel(140, 0, color);
                break;
            case SwapIndex.SECONDARY:
                secondary = color;
                swapTexture.SetPixel(242, 0, color);
                break;
        }
        swapTexture.Apply();

        if (sprite)
            this.GetComponent<SpriteRenderer>().material.SetTexture("_SwapTex", swapTexture);
        else
            this.GetComponent<Image>().material.SetTexture("_SwapTex", swapTexture);
    }

    public void LoadPlayerColors()
    {
        GameObject serverConnection = GameObject.FindGameObjectWithTag("ServerConnection");
        if (serverConnection != null)
        {
            SocketHandler socketHandler = serverConnection.GetComponent<SocketHandler>();
            if (lastCustomization != socketHandler.customization) {
                try {
                    LoadColors(socketHandler.customization);
                } catch (FormatException e) {
                    // do nothing
                }
            }
        }
    }

    public void LoadColors(CustomizationJSON json)
    {
        SwapColor(SwapIndex.SKIN, ParseColor(json.skin));
        SwapColor(SwapIndex.SHOES, ParseColor(json.shoes));
        SwapColor(SwapIndex.SHIRT, ParseColor(json.shirt));
        SwapColor(SwapIndex.BELT, ParseColor(json.belt));
        SwapColor(SwapIndex.HAT, ParseColor(json.hat));
        SwapColor(SwapIndex.HAIR, ParseColor(json.hair));
        SwapColor(SwapIndex.SLEEVES, ParseColor(json.sleeves));
        SwapColor(SwapIndex.SECONDARY, ParseColor(json.secondary));
        this.lastCustomization = json;
    }

    private Color ParseColor(string color)
    {
        string[] colorStringParts = color.Split(':');
        float red = float.Parse(colorStringParts[0]) / 255;
        float green = float.Parse(colorStringParts[1]) / 255;
        float blue = float.Parse(colorStringParts[2]) / 255;
        return new Color(red, green, blue);
    }

    private Color GetAddedColor(Color color, float r, float g, float b)
    {
        float red = Mathf.Clamp(color.r + r / 255f, 0f, 1f);
        float green = Mathf.Clamp(color.g + g / 255f, 0f, 1f);
        float blue = Mathf.Clamp(color.b + b / 255f, 0f, 1f);
        return new Color(red, green, blue, 1f);
    }

}

public enum SwapIndex
{
    SKIN,
    SHOES,
    SHIRT,
    BELT,
    HAT,
    HAIR,
    SLEEVES,
    SECONDARY
}
