﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Items/Consumable")]
public class Consumable : Item {

    public float health;
    public float armor;

    public float duration;

    public int stackSize;

    public float healthCap;
    public float armorCap;

}
