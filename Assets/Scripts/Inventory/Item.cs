﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : ScriptableObject {

    public new string name;
    public int itemId;
    public Sprite icon;
    public bool stackable;
    public bool hidden;

    public Sprite GetIcon()
    {
        return this.icon;
    }

}

/*
 * ITEM IDs:
 * 0: Empty Item
 * 1-999: Consumables
 * 1000-1999: Melee Weapons
 * 2000-2999: Bombs
 * 3000-3999: Bow
 * 4000-4999: Spells
 * 5000-5999: Ammo
 */

public enum ItemType
{
    WEAPON,
    CONSUMABLE,
    SPELL,
    BOMB,
    BOW,
    AMMO
}
