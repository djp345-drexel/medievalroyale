﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class Login : MonoBehaviour {

    public InputField emailField;
    public InputField passwordField;
    public Text outputText;

    private LoadSceneOnClick newScene;

    public Button loginButton;

    public string authenticationServer;

    private string email;

    public GameObject socketHandler;
    
    void Start () {
        newScene = new LoadSceneOnClick();
        DontDestroyOnLoad(transform.gameObject);
        loginButton.onClick.AddListener(AttemptLogin);
	}

    void AttemptLogin() {
        string email = emailField.text;
        string password = passwordField.text;
        Debug.Log("Sending form data to " + authenticationServer);
        StartCoroutine(SendLoginForm(email, password));
    }

    IEnumerator SendLoginForm(string email, string password) {
        WWWForm form = new WWWForm();
        form.AddField("email", email);
        form.AddField("password", password);
        var response = UnityWebRequest.Post(authenticationServer, form);
        yield return response.SendWebRequest();
        string resp = response.downloadHandler.text;
        if (resp == "true") {
            this.email = email;
            OnLogin();
            newScene.LoadByIndex(1);
        } else {
            Debug.Log("Invalid login credentials!");
            outputText.text = "Invalid login credentials!";
        }
    }

    void OnLogin() {
        SocketHandler socket = Instantiate(socketHandler).GetComponent<SocketHandler>();
        socket.Connect(email);
    }

}
